const connect = require("../db/connect");

module.exports = class dbController {
  // Método para obter os nomes das tabelas do banco de dados
  static async getTables(req, res) {
    // Consulta para obter a lista de tabelas
    const queryShowTables = "show tables";

    connect.query(queryShowTables, async function (err, result, fields) {
      if (err) {
        console.log(err);
        return res
          .status(500)
          .json({ error: "Erro ao obter tabelas do banco de dados" });
      }//fim do if

      // Extrai os nomes das tabelas
      const tableNames = result.map((row) => row[fields[0].name]);

      console.log("Tabelas do banco de dados: ", tableNames);

      // Retorna os nomes das tabelas
      res.status(200).json({ tables: tableNames });
    });
  }



  // Método para obter as descrições das tabelas e seus atributos
  static async getTableDescriptions(req, res) {
    // Consulta para obter a lista de tabelas
    const queryShowTables = "show tables";

    connect.query(queryShowTables, async function (err, result, fields) {
      if (err) {
        console.log(err);
        return res
          .status(500)
          .json({ error: "Erro ao obter tabelas do banco de dados" });
      }



      
      const tables = [];

      // Itera sobre os resultados para obter a descrição de cada tabela
      for (let i = 0; i < result.length; i++) {
        // Obtém o nome da tabela
        const tableName = result[i][`Tables_in_${connect.config.connectionConfig.database}`];
        
        // Consulta para obter a descrição da tabela
        const queryDescTable = `DESCRIBE ${tableName}`;

        try {
          // Executa a consulta para obter a descrição da tabela
          const tableDescription = await new Promise((resolve, reject) => {
            connect.query(queryDescTable, function (err, result, fields) {
              if (err) {
                reject(err);
              }
              resolve(result);
            });
          });

          // Adiciona o nome e a descrição da tabela ao array de tabelas
          tables.push({ name: tableName, description: tableDescription });
        } catch (error) {
          console.log(error);
          return res.status(500).json({ error: "Erro ao obter a descrição da tabela" });
        }
      }

      // Retorna as descrições das tabelas
      res.status(200).json({ message: "Obtendo todas as tabelas de suas descrições", tables });
    });
  }

 
};
