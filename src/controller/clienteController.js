//Obtendo a conexão com o banco de dados
const connect = require("../db/connect");

module.exports = class clienteController {
  static async createCliente(req, res) {
    const {
      telefone,
      nome,
      cpf,
      logradouro,
      numero,
      complemento,
      bairro,
      cidade,
      estado,
      cep,
      referencia,
    } = req.body; //pegar as informações que vêm da requisição

    //verificando se o atributo chave é diferente de 0 (zero)
    if (telefone !== 0) {
      const query = `insert into cliente (telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) values (
                    '${telefone}',
                    '${nome}',
                    '${cpf}',
                    '${logradouro}',
                    '${numero}',
                    '${complemento}',
                    '${bairro}',
                    '${cidade}',
                    '${estado}',
                    '${cep}',
                    '${referencia}'
                )`; //fim da query

      try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res
              .status(500)
              .json({ error: "Usuário não cadastrado no banco!!!" });
            return;
          }
          console.log("Inserido no Banco!!!");
          res.status(201).json({ message: "Usuário criado com sucesso!!!" });
        });
      } catch (error) {
        console.log("Erro ao executar o insert!!! - ", error);
        res.status(500).json({ error: "Erro interno do servidor!!!" });
      } //fechamento do catch
    } //fim do if
    else {
      res.status(400).json({ message: "O Telefone é obrigatório!!!" });
    } //fim do else
  } //fim do createCliente
  //select da tabela cliente

  static async getAllClientes(req, res) {
    const query = `select * from cliente`;

    try {
      connect.query(query, function (err, data) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usuários não encontrados no banco!!" });
          return;
        } //fim do if
        let clientes = data;

        console.log("Consulta realizada com sucesso!!!");
        res.status(201).json({ clientes });
      }); //fim do connect query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!!!" });
    } //fim catch
  } //fim do getAllClientes

  static async getAllClientes2(req, res) {
    //método para selecionar clientes via parâmentros específicos

    //Extrair parâmetros da consulta da URL
    const { filtro, ordenacao, ordem } = req.query;

    //Construir a consulta SQL base
    let query = `select * from cliente`;

    //Adicionar a cláusula where, quando houver
    if (filtro) {
      //query = query + filtro;  - Está incorreto
      //query = query + ` where ${filtro}`;
      query += ` where ${filtro}`;
    }

    //adicionar a clausula ordem by, quando houver
    if (ordenacao) {
      query += ` order by  ${ordenacao}`;

      //adicionar a ordem do order by (asc ou desc)
      if (ordem) {
        query += `  ${ordem}`;
      }// fim if ordem
    } // fim de ordenacao

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usuários não encontrados no banco!!" });
          return;
        } //fim do if

        console.log("Consulta realizada com sucesso!!!");
        res.status(201).json({ result });
      }); //fim do connect query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!!!" });
    } //fim catch

  } //fechamento do getAllClientes2
}; //fim do module
