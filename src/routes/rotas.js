const router = require("express").Router();
const dbController = require('../controller/dbController');
const clienteController = require ('../controller/clienteController');
const pizzariaController = require("../controller/pizzariaController");

router.get('/tables/descriptions', dbController.getTableDescriptions);
router.get('/tables', dbController.getTables);


//rota para cadastro de clientes
router.post('/cadastrocliente', clienteController.createCliente);

//rota para select da tabela cliente
router.get('/buscarclientes', clienteController.getAllClientes);

//rota para select da tabela cliente com filtro
router.get('/verclientes2', clienteController.getAllClientes2);

//rota para listar as pizzas pedidas
router.get('/listarpedido', pizzariaController.listarPedidosPizza);

//rota para listar as pizzas pedidas com join
router.get('/listarpedidocomjoin', pizzariaController.listarPedidosPizzaComJoin);

module.exports = router;
